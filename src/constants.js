
const Constants = {
    APP_ACCESS_KEY : process.env.REACT_APP_APP_ACCESS_KEY,
    APP_SECRET : process.env.REACT_APP_APP_SECRET,
    WEATHER_MASHAPE_KEY: process.env.REACT_APP_WEATHER_MASHAPE_KEY,
    GEOCODE_KEY: process.env.REACT_APP_GEOCODE_KEY
};

export default Constants




