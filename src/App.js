import React, {Component} from 'react';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCog, faTimes , faSearch, faGlobeAmericas, faFrownOpen, faCaretDown} from '@fortawesome/free-solid-svg-icons'


import Background from './components/background'
import './App.css';
library.add(
    faCog,
    faFrownOpen,
    faCaretDown,
    faTimes,
    faSearch,
    faGlobeAmericas
);

class App extends Component {
  render() {
    return (
      <div className="App">
        <Background />
      </div>
    );
  }
}

export default App;
