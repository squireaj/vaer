import React, {Component} from 'react';
import Unsplash, { toJson } from "unsplash-js";
import Constants from "../constants";

import BackgroundDisplay from './background_display'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import TextField from "@material-ui/core/TextField/TextField";
import _ from "lodash";

const unsplash = new Unsplash({
    applicationId: Constants.APP_ACCESS_KEY,
    secret: Constants.APP_SECRET,
    callbackUrl: "{CALLBACK_URL}"
});


const style = {
    close: {
        color: 'black',
        opacity: .8,
        fontSize: '21px',
        position: 'absolute',
        right: '15px',
        top: '10px'
    },
    photoHeading: {
        textAlign: 'center',
        marginBottom: '2em',
        fontSize: '15px',
        marginTop: '0px',
        position: 'relative',
        top: '2em'
    },
    textField: {
        margin: '-1px 0px 1em 1.4em'
    },
    loading:{
        width: '373px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '55vh'
    },
    icon: {
        fontSize: '50px',
        textAlign: 'center'
    },
    top: {
        height: '8em'
    },
    bottom:{
        height: '87vh',
        overflowY: 'auto',
    },
    drawer: {
        display: 'flex',
        flexDirection: 'column'
    }
};

class BackgroundSearch extends Component {
    constructor(props){
        super(props);
        this.state = {
            thumbNails : [],
            backgroundSearch: '',
            debounce: null
        };
    }

    componentDidMount(){
        unsplash.search.photos(this.state.backgroundSearch ? this.state.backgroundSearch : "popular", 1, 16)
            .then(toJson)
            .then(thumbNails => {
                this.setState({thumbNails: thumbNails.results})
            }).catch(reason => {
            console.log("Reason for catch: ", reason)
        });
        let search = (term) => {
            console.log("count", term);
            if(term){
                unsplash.search.photos(term,1, 16)
                    .then(toJson)
                    .then(thumbNails => {
                        this.setState({thumbNails: thumbNails.results})
                    }).catch(reason => {
                    console.log("Reason for catch: ", reason)
                })
            }else{
                unsplash.search.photos(this.state.backgroundSearch ? this.state.backgroundSearch : "popular",1, 16)
                    .then(toJson)
                    .then(thumbNails => {
                        this.setState({thumbNails: thumbNails.results})
                    }).catch(reason => {
                    console.log("Reason for catch: ", reason)
                })
            }
        };
        this.setState({debounce: _.debounce(search, 1000)});
    }

    search = () => {
        this.state.debounce(this.state.backgroundSearch)

    };

    handleChange = inputName => event => {
        this.setState({
            [inputName]: event.target.value,
        }, this.search)
    };


    render(){
        console.log("Thumbnails in render", this.state.thumbNails);
        const loading = <div style={style.loading}><FontAwesomeIcon style={style.icon} icon="globe-americas" spin/></div>;
        const backgroundDisplay = <BackgroundDisplay
                                        searchBackgrounds={this.search}
                                        changeBackground={this.props.changeBackground}
                                        thumbNails={this.state.thumbNails}
                                        backgroundSearch={this.state.backgroundSearch}/>;
        const id = "scrollId";
        return(
            <div style={style.drawer}>
                <div style={style.top}>
                    <FontAwesomeIcon style={style.close} onClick={this.props.toggleDrawer('right', false)} icon="times"/>
                    <h5 style={style.photoHeading}>Photos by <a onClick={()=> window.open('www.unsplash.com', "_blank")}>Unsplash</a></h5>
                    <div style={style.textField}>
                        <TextField
                            id="backgroundSearch"
                            placeholder="Search"
                            value={this.state.backgroundSearch}
                            onChange={this.handleChange('backgroundSearch')}
                            margin="normal"
                        />
                    </div>
                </div>
                <div id={id} style={style.bottom}>
                    {
                        this.state.thumbNails.length ? backgroundDisplay : loading
                    }
                </div>
            </div>
        )
    }
}

export default BackgroundSearch