import React, {Component} from 'react';
import logo from '../logo.svg';
import Drawer from '@material-ui/core/Drawer';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import BackgroundSearch from './background_search'

const style = {
    bar : {
        backgroundColor: 'black',
        opacity: 0.3,
        width: '100vw',
        height: '40px',
        position: 'absolute',
        top: 0,
        left: 0
    },
    logo : {
        width: '69px',
        top: '1px',
        left: '47.6vw',
        opacity: 1
    },
    settings : {
        color: 'white',
        opacity: .7,
        fontSize: '30px',
        position: 'absolute',
        top: '5px',
        right: '5px'
    },
    top:{
        height: '13vh'
    },
    test:{
        color: 'blue'
    }
};

class Header extends Component{

    constructor(props){
        super(props);
        this.state = {
            right: false,
            searchResults: props.searchResults,
            changeBackground: props.changeBackground,
            searchBackgrounds: props.searchBackgrounds,
        }
    }

    toggleDrawer = (side, open) => () => {
        this.setState({
            [side]: open
        });
    };

    render() {
        return (
            <div>
                <div style={style.bar}>
                <img style={style.logo} src={logo} alt="logo"/>
                </div>
                <FontAwesomeIcon style={style.settings} onClick={this.toggleDrawer('right', true)} icon="cog" />
                <Drawer anchor="right" open={this.state.right} onClose={this.toggleDrawer('right', false)}>
                    <BackgroundSearch
                        searchBackgrounds={this.props.searchBackgrounds}
                        toggleDrawer={this.toggleDrawer}
                        changeBackground={this.props.changeBackground}
                        searchResults={this.props.searchResults}/>
                </Drawer>
            </div>
        )
    }
};

export default Header