import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import TextField from "@material-ui/core/TextField/TextField";
import Constants from "../constants";
import axios from 'axios';
import Geocode from 'react-geocode';
import _ from "lodash";
import Grid from '@material-ui/core/Grid';

Geocode.setApiKey(Constants.GEOCODE_KEY);

const style = {
    card_holder: {
        padding: "1em",
        backgroundColor: "rgba(0, 0, 0, .2)",
        borderRadius: '3px',
    },
    card: {
        opacity: "1",
    },
    list: {
        marginTop: ".5rem"
    },
    header: {
        marginBottom: '0px'
    }

};


class LocationCard extends Component {
    constructor(props){
        super(props);
        this.state = {
            locationSearch: '',
            weatherData: {},
            debounce: null,
            currentLocation: '',
            locationTitle: '',
        }
    }


    componentDidMount(){
        // console.log("mashapeKey: ", Constants.WEATHER_MASHAPE_KEY);
        Geocode.fromAddress(this.props.startingLocation).then(
            response => {
                const {lat, lng } = response.results[0].geometry.location;
                console.log(lat, lng);
                axios.get(`https://iddogino-global-weather-history-v1.p.mashape.com/weather?date=2018-01-11&latitude=${lat}&longitude=${lng}`, {
                    headers: {
                        "X-Mashape-Key": Constants.WEATHER_MASHAPE_KEY
                    }
                })
                    .then(result => {
                        this.setState({weatherData: result.data});
                        this.setState({locationTitle: this.state.locationSearch});
                        console.log("results", result.data);
                    }).catch(reason => {
                    console.log("Reason for catch: ", reason)
                });

            },
            error => {
                console.log('error in Geocode', error)
            }
        );

        let search = (term) => {
            console.log('locationSearchTerm', term);
            if(term){
                Geocode.fromAddress(term).then(
                    response => {
                        const {lat, lng } = response.results[0].geometry.location;
                        console.log(lat, lng);
                        axios.get(`https://iddogino-global-weather-history-v1.p.mashape.com/weather?date=2018-01-11&latitude=${lat}&longitude=${lng}`, {
                            headers: {
                                "X-Mashape-Key": Constants.WEATHER_MASHAPE_KEY
                            }
                        })
                            .then(result => {
                                this.setState({weatherData: result.data});
                                this.setState({locationTitle: this.state.locationSearch});
                                console.log("results", result.data);
                            }).catch(reason => {
                               console.log("Reason for catch: ", reason)
                            });

                    },
                    error => {
                        console.log('error in Geocode', error)
                    }
                )
            }else {
                Geocode.fromAddress(this.props.startingLocation).then(
                    response => {
                        const {lat, lng } = response.results[0].geometry.location;
                        console.log(lat, lng);
                        axios.get(`https://iddogino-global-weather-history-v1.p.mashape.com/weather?date=2018-01-11&latitude=${lat}&longitude=${lng}`, {
                            headers: {
                                "X-Mashape-Key": Constants.WEATHER_MASHAPE_KEY
                            }
                        })
                            .then(result => {
                                this.setState({weatherData: result.data});
                                this.setState({locationTitle: this.state.locationSearch});
                                console.log("results", result.data);
                            }).catch(reason => {
                            console.log("Reason for catch: ", reason)
                        });

                    },
                    error => {
                        console.log('error in Geocode', error)
                    }
                )
            }
        };
        this.setState({debounce: _.debounce(search, 1000)});
    }

    search = () => {
      this.state.debounce(this.state.locationSearch)
    };

    handleChange = input => event => {
        this.setState({
            [input]: event.target.value
        }, this.search)
    };

    render(){
        return(
            <div style={style.card_holder}>
                <Card>
                    <CardContent>
                        <div style={style.card}>
                            {/*<FontAwesomeIcon style={style.icon} icon="search"/>*/}
                            <Grid container direction="column">
                                <h2 style={style.header}>{this.state.locationTitle ? this.state.locationTitle : this.props.startingLocation}</h2>
                                <TextField
                                    id="locationSearch"
                                    placeholder="Change Location"
                                    value={this.state.locationSearch}
                                    onChange={this.handleChange('locationSearch')}
                                    margin="normal"
                                />
                                <div style={style.list}>
                                    <Grid container justify="space-between">
                                        <div>High:</div>
                                        <div>{parseInt(this.state.weatherData.TMAX, 10) * 9/5 + 32 }&deg;</div>
                                    </Grid>
                                    <Grid container justify="space-between">
                                        <div>Low:</div>
                                        <div>{this.state.weatherData.TMIN * 9/5 + 32}&deg;</div>
                                    </Grid>
                                </div>
                            </Grid>
                        </div>
                    </CardContent>
                </Card>
            </div>
        )
    }


}

export default LocationCard