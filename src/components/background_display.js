import React, {Component} from 'react';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import InfinateScroll from 'react-infinite-scroll-component';
import Unsplash, { toJson } from "unsplash-js";
import Constants from "../constants";

const unsplash = new Unsplash({
    applicationId: Constants.APP_ACCESS_KEY,
    secret: Constants.APP_SECRET,
    callbackUrl: "{CALLBACK_URL}"
});


const style = {
    media: {
        height: '9em',
        width: '9em'

    },
    card: {
        marginBottom: '1em',
        width: '9em',
        float: 'left',
        marginLeft: '1.95em'
    },
    display: {
        width: '22.3em'
    },
    loading:{
        width: '373px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        height: '20vh'
    },
    icon: {
        fontSize: '50px',
        textAlign: 'center'
    },
    scroll:{
    }
};

class BackgroundDisplay extends Component {
    constructor(props){
        super(props);
        this.state={
            thumbNails: [],
            originalThumbNails: this.props.thumbNails,
            lazyLoadCount: 1
        };
        console.log('thumbNails in background display: ', this.props.thumbNails);
    }


    componentDidMount(){
        // console.log("running again");
        this.setState({thumbNails: this.props.thumbNails.map(i => {
            return (
                <Card onClick={this.props.changeBackground(i.urls.raw)} style={style.card} key={i.id}>
                    <CardMedia
                        style={style.media}
                        image={i.urls.thumb}
                    />
                </Card>
            )
        })});
    }
    componentWillReceiveProps(nextProps) {
        console.log("running again");
        this.setState({thumbNails: nextProps.thumbNails.map(i => {
                return (
                    <Card onClick={nextProps.changeBackground(i.urls.raw)} style={style.card} key={i.id}>
                        <CardMedia
                            style={style.media}
                            image={i.urls.thumb}
                        />
                    </Card>
                )
            })});
    }


    fetchMoreData = () => {
        this.setState({lazyLoadCount: this.state.lazyLoadCount + 1});
        console.log("fetchMoreData", this.state.lazyLoadCount);
        console.log("this.props.backgroundSearch", this.props.backgroundSearch);
        unsplash.search.photos(this.props.backgroundSearch ? this.props.backgroundSearch : "popular", this.state.lazyLoadCount, 16)
            .then(toJson)
            .then(thumbNails => {
                const newThumbs = thumbNails.results.map(i => {
                        return (
                            <Card onClick={this.props.changeBackground(i.urls.raw)} style={style.card} key={i.id}>
                                <CardMedia
                                    style={style.media}
                                    image={i.urls.thumb}
                                />
                            </Card>
                        )
                    });
                let newArr = [...this.state.thumbNails, ...newThumbs];
                this.setState({thumbNails: newArr})
            }).catch(reason => {
            console.log("Reason for catch: ", reason)
        });
    };

    render(){

        const loading = <div style={style.loading}><FontAwesomeIcon style={style.icon} icon="globe-americas" spin/></div>;
        const end = <div style={style.loading}><FontAwesomeIcon style={style.icon} icon="globe-americas" spin/></div>;

        if(!this.state.thumbNails.length){
            return loading
        }else{
            return (
                <div>
                    <InfinateScroll
                        dataLength={this.state.thumbNails.length}
                        next={this.fetchMoreData}
                        hasMore={true}
                        loader={loading}
                        scrollableTarget={"scrollId"}
                        scrollThreshold={0.99}
                        endMessage={end}
                        initialScrollY={0}

                    >
                        <div style={style.display}>
                            {this.state.thumbNails}
                        </div>
                    </InfinateScroll>
                </div>
            )
        }
    }
}

export default BackgroundDisplay