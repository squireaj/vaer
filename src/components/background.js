import React, {Component} from 'react';
import Header from './header';
import LocationCard from './location_card';
import Grid from '@material-ui/core/Grid';

const style = {
  location_card: {
      marginTop: '.5em'
  }
};

class Background extends Component{
    constructor(props){
        super(props);
        this.state = {
            background: 'https://images.unsplash.com/photo-1515224526905-51c7d77c7bb8',
            searchResults: [],
            debounce: null
        };
    }

    handleClick = (background) => (e) => {
    this.setState({background});
    };

    render() {

    const backgroundStyle = {
        backgroundImage : `url('${this.state.background}')`,
        height: '100vh',
        width: '100vw',
        backgroundSize: 'cover',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    };
    return (
        <div style={backgroundStyle}>
            <Header
                searchBackgrounds={this.state.debounce}
                changeBackground={this.handleClick}
                searchResults={this.state.searchResults}/>
            <Grid container justify="space-evenly">
                <Grid style={style.location_card} item xs={10} m={6} lg={3}>
                    <LocationCard startingLocation={"New York"}/>
                </Grid>
                <Grid style={style.location_card} item xs={10} m={6} lg={3}>
                    <LocationCard startingLocation={"San Fransisco"}/>
                </Grid>
            </Grid>
        </div>
    )

    }
};

export default Background